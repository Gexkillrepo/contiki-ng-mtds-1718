#include "contiki.h"
#include "mqtt.h"

#include "contiki.h"
#include "mqtt.h"
#include "rpl.h"
#include "net/netstack.h"
#include "net/ipv6/uip.h"
#include "net/ipv6/sicslowpan.h"
#include "lib/random.h"
#include "sys/clock.h"
#include "sys/etimer.h"
#include "sys/ctimer.h"
#include "sys/energest.h"
#include "leds.h"

/* Log configuration */
#include "sys/log.h"
#define LOG_MODULE "BR MQTT-CONN"
#define LOG_LEVEL LOG_LEVEL_INFO

#include <stdio.h>  /* For printf() */

/*---------------------------------------------------------------------------*/
PROCESS(mqtt_gen_info_pub, "Border router pub process");
/*---------------------------------------------------------------------------*/
/*
 * Number of times to try reconnecting to the broker.
 * Can be a limited number (e.g. 3, 10 etc) or can be set to RETRY_FOREVER
 */
static struct timer connection_life;
static uint8_t connect_attempt;
/*---------------------------------------------------------------------------*/
/* Various states */
static uint8_t state;
/*---------------------------------------------------------------------------*/
/*
 * Buffers for Client ID and Topic.
 * Make sure they are large enough to hold the entire respective string
 *
 * We also need space for the null termination
 */
static char client_id[BUFFER_SIZE];
static char pub_topic[BUFFER_SIZE];
/*---------------------------------------------------------------------------*/
/*
 * The main MQTT buffers.
 * We will need to increase if we start publishing more data.
 */
static struct mqtt_connection conn;
static char app_buffer[APP_BUFFER_SIZE];
/*---------------------------------------------------------------------------*/
static struct etimer publish_periodic_timer;
static struct ctimer ct;
static char *buf_ptr;
static uint16_t seq_nr_value = 0;
/*---------------------------------------------------------------------------*/
static void
publish_led_off(void *d)
{
  leds_off(MQTT_STATUS_LED);
}
/*---------------------------------------------------------------------------*/
static void
mqtt_event(struct mqtt_connection *m, mqtt_event_t event, void *data)
{
  switch(event) {
    case MQTT_EVENT_CONNECTED: {
      LOG_INFO("Application has a MQTT connection!\n");
      timer_set(&connection_life, CONNECTION_STABLE_TIME);
      state = STATE_PUBLISHING;
      break;
    }
    case MQTT_EVENT_DISCONNECTED: {
      LOG_INFO("MQTT Disconnect: reason %u\n", *((mqtt_event_t *)data));

      state = STATE_DISCONNECTED;
      process_poll(&mqtt_gen_info_pub);
      break;
    }
    case MQTT_EVENT_PUBACK: {
      LOG_INFO("Publishing complete\n");
      break;
    }
    default:
    LOG_WARN("Application got a unhandled MQTT event: %i\n", event);
    break;
  }
}
/*---------------------------------------------------------------------------*/
static int
construct_pub_topic(void)
{
  int len = snprintf(pub_topic, BUFFER_SIZE, MQTT_PUBLISH_TOPIC);

  /* len < 0: Error. Len >= BUFFER_SIZE: Buffer too small */
  if(len < 0 || len >= BUFFER_SIZE) {
    LOG_ERR("Pub topic: %d, buffer %d\n", len, BUFFER_SIZE);
    return 0;
  }

  return 1;
}
/*---------------------------------------------------------------------------*/
static int
construct_client_id(void)
{
  int len = snprintf(client_id, BUFFER_SIZE, "d:%s:%s:%02x%02x%02x%02x%02x%02x",
   DEFAULT_ORG_ID, DEFAULT_TYPE_ID,
   linkaddr_node_addr.u8[0], linkaddr_node_addr.u8[1],
   linkaddr_node_addr.u8[2], linkaddr_node_addr.u8[5],
   linkaddr_node_addr.u8[6], linkaddr_node_addr.u8[7]);

  /* len < 0: Error. Len >= BUFFER_SIZE: Buffer too small */
  if(len < 0 || len >= BUFFER_SIZE) {
    LOG_INFO("Client ID: %d, Buffer %d\n", len, BUFFER_SIZE);
    return 0;
  }

  return 1;
}
/*---------------------------------------------------------------------------*/
static void
update_config(void)
{
  if(construct_client_id() == 0) {
    /* Fatal error. Client ID larger than the buffer */
    state = STATE_CONFIG_ERROR;
    return;
  }

  if(construct_pub_topic() == 0) {
    /* Fatal error. Topic larger than the buffer */
    state = STATE_CONFIG_ERROR;
    return;
  }

  /* Reset the counter */
  seq_nr_value = 0;

  state = STATE_INIT;

  /*
   * Schedule next timer event ASAP
   *
   * If we entered an error state then we won't do anything when it fires.
   *
   * Since the error at this stage is a config error, we will only exit this
   * error state if we get a new config.
   */
  etimer_set(&publish_periodic_timer, 0);

  return;
}
/*---------------------------------------------------------------------------*/
static void
publish(void)
{
  mqtt_status_t conn_status;
  /* Publish MQTT topic */
  int len;
  int remaining = APP_BUFFER_SIZE;

  seq_nr_value++;

  buf_ptr = app_buffer;

    len = snprintf(buf_ptr, remaining,
     "{"
     "\"Payload\":{"
     "\"Border_Router_ID\":%d,"
     "\"Device_Type\":\"%s\"",
     BORDER_ROUTER_ID,
     DEFAULT_TYPE_ID); 

  if(len < 0 || len >= remaining) {
    LOG_ERR("Buffer too short. Have %d, need %d + \\0\n", remaining, len);
    return;
  }

  remaining -= len;
  buf_ptr += len;

  len = snprintf(buf_ptr, remaining, "}}");

  if(len < 0 || len >= remaining) {
    LOG_ERR("Buffer too short. Have %d, need %d + \\0\n", remaining, len);
    return;
  }

  conn_status = mqtt_publish(&conn, NULL, pub_topic, (uint8_t *)app_buffer,
   strlen(app_buffer), MQTT_QOS_LEVEL_0, MQTT_RETAIN_OFF);
  LOG_INFO("State of mqtt_publish: %i\n", conn_status);

  LOG_INFO("Publish sent out!\n");
}
/*---------------------------------------------------------------------------*/
static void
connect_to_broker(void)
{
  mqtt_status_t conn_status;

  /* Connect to MQTT server */
  conn_status = mqtt_connect(&conn, MQTT_BROKER_IP_ADDR, DEFAULT_BROKER_PORT,
   BORDER_ROUTER_PUBLISHING * 3);
  LOG_INFO("State of mqtt_connect: %i\n", conn_status);

  state = STATE_CONNECTING;
}
/*---------------------------------------------------------------------------*/
static void
state_machine(void)
{
  mqtt_status_t conn_status;

  switch(state) {
    case STATE_INIT:

    
    /* If we have just been configured register MQTT connection */
    conn_status = mqtt_register(&conn, &mqtt_gen_info_pub, client_id, mqtt_event,
      MAX_TCP_SEGMENT_SIZE);
    LOG_INFO("State of mqtt_register: %i\n", conn_status);

    // mqtt_set_username_password(&conn, "use-token-auth",
    //  DEFAULT_AUTH_TOKEN);

    /* _register() will set auto_reconnect. We don't want that. */
    conn.auto_reconnect = 0;
    connect_attempt = 1;

    state = STATE_REGISTERED;
    LOG_INFO("Init\n");
    /* Continue */
    case STATE_REGISTERED:
    if(uip_ds6_get_global(ADDR_PREFERRED) != NULL) {
      /* Registered and with a global IPv6 address, connect! */
      LOG_INFO("Joined network! Connect attempt %u\n", connect_attempt);
      connect_to_broker();
    } else {
      leds_on(MQTT_STATUS_LED);
      ctimer_set(&ct, NO_NET_LED_DURATION, publish_led_off, NULL);
    }
    etimer_set(&publish_periodic_timer, NET_CONNECT_PERIODIC);
    return;
    break;
    case STATE_CONNECTING:
    leds_on(MQTT_STATUS_LED);
    ctimer_set(&ct, CONNECTING_LED_DURATION, publish_led_off, NULL);
    /* Not connected yet. Wait */
    LOG_INFO("Connecting: retry %u...\n", connect_attempt);
    break;
    case STATE_PUBLISHING:
    /* If the timer expired, the connection is stable. */
    if(timer_expired(&connection_life)) {
      /*
       * Intentionally using 0 here instead of 1: We want RECONNECT_ATTEMPTS
       * attempts if we disconnect after a successful connect
       */
      connect_attempt = 0;
    }

    if(mqtt_ready(&conn) && conn.out_buffer_sent) {
      leds_on(MQTT_STATUS_LED);
      ctimer_set(&ct, PUBLISH_LED_ON_DURATION, publish_led_off, NULL);
      publish();


      etimer_set(&publish_periodic_timer, BORDER_ROUTER_PUBLISHING);

      /* Return here so we don't end up rescheduling the timer */
      return;
    } else {
      /*
       * Our publish timer fired, but some MQTT packet is already in flight
       * (either not sent at all, or sent but not fully ACKd).
       *
       * This can mean that we have lost connectivity to our broker or that
       * simply there is some network delay. In both cases, we refuse to
       * trigger a new message and we wait for TCP to either ACK the entire
       * packet after retries, or to timeout and notify us.
       */
      LOG_INFO("Publishing... (MQTT state=%d, q=%u)\n", conn.state,
        conn.out_queue_full);
    }
    break;
    case STATE_DISCONNECTED:
    LOG_INFO("Disconnected\n");
    if(connect_attempt < RECONNECT_ATTEMPTS ||
     RECONNECT_ATTEMPTS == RETRY_FOREVER) {
      /* Disconnect and backoff */
      clock_time_t interval;
    mqtt_disconnect(&conn);
    connect_attempt++;

    interval = connect_attempt < 3 ? RECONNECT_INTERVAL << connect_attempt :
    RECONNECT_INTERVAL << 3;

    LOG_INFO("Disconnected: attempt %u in %lu ticks\n", connect_attempt, interval);

    etimer_set(&publish_periodic_timer, interval);

    state = STATE_REGISTERED;
    return;
  } else {
      /* Max reconnect attempts reached. Enter error state */
    state = STATE_ERROR_2;
    LOG_ERR("Aborting connection after %u attempts\n", connect_attempt - 1);
  }
  break;
  case STATE_CONFIG_ERROR:
    /* Idle away. The only way out is a new config */
  LOG_ERR("Bad configuration.\n");
  return;
  case STATE_ERROR_2:
  default:
  leds_on(MQTT_STATUS_LED);
    /*
     * 'default' should never happen.
     *
     * If we enter here it's because of some error. Stop timers. The only thing
     * that can bring us out is a new config event
     */
  LOG_INFO("Default case: State=0x%02x\n", state);
  return;
}

  /* If we didn't return so far, reschedule ourselves */
etimer_set(&publish_periodic_timer, STATE_MACHINE_PERIODIC);
}

PROCESS_THREAD(mqtt_gen_info_pub, ev, data)
{ 
  PROCESS_BEGIN();

  LOG_INFO("MQTT Server Process\n");
  LOG_INFO("Border Router ID: %d\n", BORDER_ROUTER_ID);

  update_config();

  /* Main loop */
  while(1) {
    PROCESS_YIELD();

    if (ev == PROCESS_EVENT_TIMER && data == &publish_periodic_timer) {
      state_machine();
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/