#include "contiki.h"
#include "border-router-common.h"
#include "rpl-dag-root.h"
#include "net/ipv6/simple-udp.h"

/*---------------------------------------------------------------------------*/

/* Log configuration */
#include "sys/log.h"
#define LOG_MODULE "BR"
#define LOG_LEVEL LOG_LEVEL_INFO

uint8_t prefix_set;
/*---------------------------------------------------------------------------*/
PROCESS_NAME(mqtt_gen_info_pub);
/*---------------------------------------------------------------------------*/
void
print_local_addresses(void)
{
  int i;
  uint8_t state;

  LOG_INFO("Server IPv6 addresses:\n");
  for(i = 0; i < UIP_DS6_ADDR_NB; i++) {
    state = uip_ds6_if.addr_list[i].state;
    if(uip_ds6_if.addr_list[i].isused &&
       (state == ADDR_TENTATIVE || state == ADDR_PREFERRED)) {
      LOG_INFO("  ");
      LOG_INFO_6ADDR(&uip_ds6_if.addr_list[i].ipaddr);
      LOG_INFO_("\n");
    }
  }

  /*---------------------------------------------------------------------------*/
  /*
   * It is started the protothread responsible for the publishing 
   * of messages with the border router ID in the payload.
   */
  process_start(&mqtt_gen_info_pub, (NULL));
  /*---------------------------------------------------------------------------*/

}
/*---------------------------------------------------------------------------*/
void
set_prefix_64(uip_ipaddr_t *prefix_64)
{
  prefix_set = 1;
  rpl_dag_root_init(prefix_64, NULL);
  rpl_dag_root_init_dag_immediately();
}
/*---------------------------------------------------------------------------*/