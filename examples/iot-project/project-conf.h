#ifndef PROJECT_CONF_H_
#define PROJECT_CONF_H_

/*---------------------------------------------------------------------------*/
/* Enable TCP */
#define UIP_CONF_TCP 1
/*---------------------------------------------------------------------------*/
/* Disable PROP_MODE */
#define CC13XX_CONF_PROP_MODE 0

/* Name of MQTT topic */
#define MQTT_PUBLISH_TOPIC "/iot/border router"
#define MQTT_SUBSCRIBE_TOPIC "/iot/border router/info/json"

#define MAX_RANDOM_VALUE  40

#define MOBILE_DEVICE_ID	1

/* Threshold of acceleration */
#define T 10

/* Periodical time units before publishing an MQTT message */
/* or each time we try to publish */
#define K (10 * CLOCK_SECOND)

/* Time units before return to collect other samples */
/* interrupting the connection with the MQTT broker */
#define G (150 * CLOCK_SECOND)

#define SAMPLING_DELAY CLOCK_SECOND
#define ENERGEST_DELAY (10 * CLOCK_SECOND)

#define MQTT_STATUS_LED  LEDS_GREEN
#define MQTT_TRIGGER_LED LEDS_RED

#define MQTT_BROKER_IP_ADDR "fd00::1"
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/*
 * A timeout used when waiting for something to happen (e.g. to connect or to
 * disconnect)
 */
#define STATE_MACHINE_PERIODIC     (CLOCK_SECOND >> 1)
/*---------------------------------------------------------------------------*/
/* Provide visible feedback via LEDS during various states */
/* When connecting to broker */
#define CONNECTING_LED_DURATION    (CLOCK_SECOND >> 2)

/* Each time we try to publish */
#define PUBLISH_LED_ON_DURATION    (CLOCK_SECOND)
/*---------------------------------------------------------------------------*/
/* Connections and reconnections */
#define RETRY_FOREVER              0xFF
#define RECONNECT_INTERVAL         (CLOCK_SECOND * 2)
/*---------------------------------------------------------------------------*/
/*
 * Number of times to try reconnecting to the broker.
 * Can be a limited number (e.g. 3, 10 etc) or can be set to RETRY_FOREVER
 */
#define RECONNECT_ATTEMPTS         RETRY_FOREVER
#define CONNECTION_STABLE_TIME     (CLOCK_SECOND * 5)
/*---------------------------------------------------------------------------*/
/* Various states */
#define STATE_INIT            0
#define STATE_REGISTERED      1
#define STATE_CONNECTING      2
#define STATE_CONNECTED       3
#define STATE_PUBLISHING      4
#define STATE_DISCONNECTED    5
#define STATE_NEWCONFIG       6
#define STATE_CONFIG_ERROR    0xFE
#define STATE_ERROR_2         0xFF
/*---------------------------------------------------------------------------*/
/* A timeout used when waiting to connect to a network */
#define NET_CONNECT_PERIODIC        (CLOCK_SECOND >> 2)
#define NO_NET_LED_DURATION         (NET_CONNECT_PERIODIC >> 1)
/*---------------------------------------------------------------------------*/
/* Default configuration values */

/*
 * Publish to a local MQTT broker (e.g. mosquitto) running on
 * the node that hosts your border router
 */
#define DEFAULT_ORG_ID              "mqtt-client"
#define DEFAULT_TYPE_ID             "cc26xx"
// #define DEFAULT_AUTH_TOKEN          "AUTHZ"
#define DEFAULT_SUBSCRIBE_CMD_TYPE  "+"
#define DEFAULT_BROKER_PORT         1883
#define DEFAULT_KEEP_ALIVE_TIMER    60
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
/* Maximum TCP segment size for outgoing segments of our socket */
#define MAX_TCP_SEGMENT_SIZE    32
/*---------------------------------------------------------------------------*/
/*
 * Buffers for Client ID and Topic.
 * Make sure they are large enough to hold the entire respective string
 *
 * We also need space for the null termination
 */
#define BUFFER_SIZE 64
/*---------------------------------------------------------------------------*/
/*
 * The main MQTT buffers.
 * We will need to increase if we start publishing more data.
 */
#define APP_BUFFER_SIZE 512
/*---------------------------------------------------------------------------*/
/* Enable ENERGEST */
#define ENERGEST_CONF_ON 1

/* Energy consumption variables */
// E = C * V * t = (C * V) * t =  P * t

#define PREFIX_MICRO      1e-6
#define PREFIX_MILLI      1e-3
// 3.0 V
#define VDDS              3.0
// (3.0 V * 6.1mA) -> 18.3 mW
#define POWER_RX          VDDS * (PREFIX_MILLI * 6.1)
// (3.0 V * 9.1mA) -> 27.3 mW
#define POWER_TX          VDDS * (PREFIX_MILLI * 9.1)
 // CLOCK_SPEED_MAX = 48MHz of an ARM Cortex M3 CPU
#define CLOCK_MAXSPEED    48
// 3.0 V * (1.45mA + 31uA/MHz + 31uA/MHz * 48 MHz) -> 3.0 V * 2.938 mA -> 8.814 mW
#define POWER_ACTIVE_CPU  VDDS * (PREFIX_MILLI * 1.45 + (PREFIX_MICRO * 31) * CLOCK_MAXSPEED)
// 3.0 V * 550uA -> 1.65 mW - NOT USED
// #define POWER_SLEEP_CPU   VDDS * (PREFIX_MILLI * 0.55)
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
#endif /* PROJECT_CONF_H_ */
/*---------------------------------------------------------------------------*/
