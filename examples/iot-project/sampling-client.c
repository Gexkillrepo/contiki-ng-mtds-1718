/*---------------------------------------------------------------------------*/
/** 
 * \sampling-client.c
 * Process for a Mobile Device that collects fake-acceleration samples
 * generated as random numbers.
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include "lib/random.h"
#include "sys/clock.h"
#include "sys/etimer.h"
#include "net/netstack.h"

#include "sys/log.h"
#define LOG_MODULE "SAMPLING-CLIENT"
#define LOG_LEVEL LOG_LEVEL_INFO

#include <stdio.h>  /* For printf() */
#include <string.h>
/*---------------------------------------------------------------------------*/
PROCESS(sampling_client_process, "Sampling client process");
PROCESS_NAME(mqtt_client_process);
PROCESS_NAME(tsch_process);
PROCESS_NAME(tsch_send_eb_process);
PROCESS_NAME(tsch_pending_events_process);
/*---------------------------------------------------------------------------*/
/*
 * structure used to pass data between the sampling_client_process
 * and the mqtt_client_process.
 */
typedef struct mqtt_response_packet {
  unsigned short acceleration_mean;
  unsigned short mqtt_border_router_id;
  clock_time_t estimatedG;
} mqtt_response_packet_t;

/*---------------------------------------------------------------------------*/
PROCESS_THREAD(sampling_client_process, ev, data)
{
  /* Initialization of the needed parameters */

  // structure passed around the protothreads.
  static mqtt_response_packet_t border_router_packet;

  // timer used to generate the random data.
  static struct etimer et;
  
  // It is used to store the actual collected random sample.
  static short sample;

  // It is used to sum up all the samples.
  static short tot_amount;

  // It counts how many samples are generated, to do an average afterwards
  // when it is produced a data below the threshold T.
  static short tot_counter;

  // It is used to to get all the running protothreads.
  static struct process *p;

  // It is used to check if a protothead is already spawned or not.
  static unsigned short check;
  
  PROCESS_BEGIN();

  LOG_INFO("Mobile Device ID: %d\n", MOBILE_DEVICE_ID);
  /*
   * It is set the initial values for the structure passed around.
   */
  LOG_INFO("Initial data structure configuration.\n");
  border_router_packet.acceleration_mean = 0;
  border_router_packet.mqtt_border_router_id = 0;
  border_router_packet.estimatedG = 0;

  /* The protothreads responsible for the functioning
   * TSCH protocl are stopped to allow the radio to be
   * switched off.
   */
  process_exit(&tsch_pending_events_process);
  process_exit(&tsch_send_eb_process);
  process_exit(&tsch_process);

  tot_amount = 0;
  tot_counter = 0;


  etimer_set(&et, SAMPLING_DELAY);

  /* The radio is switched off */
  NETSTACK_RADIO.off();

  while(1) {
    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));

    /* Random values generation */
    random_init((unsigned) clock_time());
    sample = random_rand() % MAX_RANDOM_VALUE;
    tot_amount += sample;
    tot_counter += 1;

    printf("Acceleration sample collected: %d\n", sample);

    etimer_reset(&et);

    /* 
     * The sample collected is below the theshold, 
     * the user is still standing.
     */
    if (sample < T) {

      border_router_packet.acceleration_mean = tot_amount/tot_counter;

/*---------------------------------------------------------------------------*/
    /* 
     * It is checked if the mqtt protothread already exist, if so
     * this is not the first iteration of the loop and it is resumed.
     * Otherwise it is started.
     */
      p = PROCESS_LIST();

      check = 0;
      while(p->next != NULL) {

        if (strcmp(p->name, "MQTT Client process" ) == 0) {
          process_post(&mqtt_client_process, 
            PROCESS_EVENT_POLL, &border_router_packet);
          check = 1;
          break;
        }

        p = p->next;
      }

      if (check == 0) process_start(&mqtt_client_process, &border_router_packet);
/*---------------------------------------------------------------------------*/
      /* 
       * It is stopped the sampling prototrhead, 
       * until another one resumes it.
       */
      PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_POLL && data != NULL);
      LOG_INFO("sampling-client protothread resumed\n");

      border_router_packet = * (mqtt_response_packet_t *) data;

      tot_amount = 0;
      tot_counter = 0;
      etimer_set(&et, SAMPLING_DELAY); /* Trigger a 1s timer */

    }

  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
/**
 * @}
 * @}
 **/
 