/*---------------------------------------------------------------------------*/
/** 
 * \mqtt-client.c
 * Process for a Mobile Device that established a new connection with the
 * border router and the mqtt broker, enabling TSCH.
 * It is also responsible for the retrieving of the border router id through
 * the subscription over fixed topic and finally it publishes periodically
 * mqtt messages until G doesn't expires.
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include "mqtt.h"
#include "rpl.h"
#include "net/netstack.h"
#include "net/ipv6/uip.h"
#include "net/ipv6/sicslowpan.h"
#include "sys/clock.h"
#include "sys/etimer.h"
#include "sys/ctimer.h"
#include "sys/energest.h"
#include "leds.h"

#include "sys/log.h"
#define LOG_MODULE "MQTT-CLIENT"
#define LOG_LEVEL LOG_LEVEL_INFO

#include <string.h>
#include <stdio.h>  /* For printf() */
/*---------------------------------------------------------------------------*/
/*
 * Number of times to try reconnecting to the broker.
 * Can be a limited number (e.g. 3, 10 etc) or can be set to RETRY_FOREVER
 */
static struct timer connection_life;
static uint8_t connect_attempt;
/*---------------------------------------------------------------------------*/
/* Various states */
static uint8_t state;
/*---------------------------------------------------------------------------*/
PROCESS_NAME(sampling_client_process);
PROCESS_NAME(tsch_process);
PROCESS_NAME(tsch_send_eb_process);
PROCESS_NAME(tsch_pending_events_process);
PROCESS(mqtt_client_process, "MQTT Client process");
/*---------------------------------------------------------------------------*/
/*
 * Buffers for Client ID and Topic.
 * Make sure they are large enough to hold the entire respective string
 *
 * We also need space for the null termination
 */
static char client_id[BUFFER_SIZE];
static char pub_topic[BUFFER_SIZE];
static char sub_topic[BUFFER_SIZE];
/*---------------------------------------------------------------------------*/
/*
 * The main MQTT buffers.
 * We will need to increase if we start publishing more data.
 */
static struct mqtt_connection conn;
static char app_buffer[APP_BUFFER_SIZE];
/*---------------------------------------------------------------------------*/
static struct mqtt_message *msg_ptr = 0;
static struct etimer publish_periodic_timer;
static struct ctimer ct;
static char *buf_ptr;
static uint16_t seq_nr_value = 0;
/*---------------------------------------------------------------------------*/
/* Pass this structure between sampling-client and mqtt-client.
 * Basically it contains the information retrieved from the border router
 * plus the current estimate of G.
 */
typedef struct mqtt_response_packet {
  unsigned short acceleration_mean;
  unsigned short mqtt_border_router_id;
  clock_time_t estimatedG;
} mqtt_response_packet_t;

static mqtt_response_packet_t border_router_packet;

static struct etimer user_return_to_move;

static unsigned long temptime;
static unsigned long conntime;
static unsigned long estimationtime;
static unsigned long totaltime;
static unsigned long totalcounter;
/*---------------------------------------------------------------------------*/
/*
 * Procedures used to get a parameter from a Json string.
 */
static void getvalue(char buffer [], const char * json, char * key) {

  int len = strlen(json);
  char buf[len];

  const char d1[2] = "{";
  const char d2[2] = "}";
  const char d3[2] = ",";
  const char d4[2] = ":";

  char * token;

  strcpy(buf, json);
  token = strstr(buf,key);
  token = strtok(token, d1);
  token = strtok(token, d2);
  token = strtok(token, d3);
  token = strtok(token, d4);
  token = strtok(NULL, d4);

  strcpy(buffer, token); 
}

static unsigned short
get_param(const uint8_t *chunk, char * key)
{

  // LOG_INFO("JSON string: %s\n", chunk);

  const char * json = (const char *) chunk;
  char buf[100];

  getvalue(buf, json, key);
  // LOG_INFO("{ KEY: %s, VALUE: %d }\n", key, atoi(buf));

  return atoi(buf);
}
/*---------------------------------------------------------------------------*/
/*
 * Procedure used to get and format correctly the IPv6 address 
 * of the mobile device.
 */
int
ipaddr_snprintf(char *buf, uint8_t buf_len, const uip_ipaddr_t *addr)
{
  uint16_t a;
  uint8_t len = 0;
  int i, f;
  for(i = 0, f = 0; i < sizeof(uip_ipaddr_t); i += 2) {
    a = (addr->u8[i] << 8) + addr->u8[i + 1];
    if(a == 0 && f >= 0) {
      if(f++ == 0) {
        len += snprintf(&buf[len], buf_len - len, "::");
      }
    } else {
      if(f > 0) {
        f = -1;
      } else if(i > 0) {
        len += snprintf(&buf[len], buf_len - len, ":");
      }
      len += snprintf(&buf[len], buf_len - len, "%x", a);
    }
  }

  return len;
}
/*---------------------------------------------------------------------------*/
/*
 * Procedure used to trigger the green led of the launchpad when a message
 * is published.
 */
static void
publish_led_off(void *d)
{
  leds_off(MQTT_STATUS_LED);
}
/*---------------------------------------------------------------------------*/
/*
 * Procedure used by the mobile device to unsubscribe from a topic.
 */
static void
unsubscribe(void)
{
  mqtt_status_t status;

  status = mqtt_unsubscribe(&conn, NULL, sub_topic);

  LOG_INFO("Unsubscribing\n");
  if(status == MQTT_STATUS_OUT_QUEUE_FULL) {
    LOG_INFO("Tried to unsubscribe but command queue was full!\n");
  }
}
/*---------------------------------------------------------------------------*/
/*
 * Procedure that constructs at runtime the publishing topic.
 */
static int
construct_pub_topic(void)
{
  int len = snprintf(pub_topic, BUFFER_SIZE,"%s/%d/json",
    MQTT_PUBLISH_TOPIC, border_router_packet.mqtt_border_router_id);

  /* len < 0: Error. Len >= BUFFER_SIZE: Buffer too small */
  if(len < 0 || len >= BUFFER_SIZE) {
    LOG_ERR("Pub topic: %d, buffer %d\n", len, BUFFER_SIZE);
    return 0;
  }

  return 1;
}
/*---------------------------------------------------------------------------*/
/*
 * Procedure that extracts the border router ID from the Json received
 * from the mqtt broker.
 * It is called when the mobile device receivers a response to a subscription,
 * that is a publication message from the border router.
 */
static void
pub_handler(const char *topic, uint16_t topic_len, const uint8_t *chunk,
  uint16_t chunk_len)
{
  LOG_INFO("Pub handler: topic='%s' (len=%u), chunk_len=%u\n", topic, topic_len,
    chunk_len);

  // LOG_INFO("Pub chunk message: %s\n", chunk);

  /* Set parameters for publishing messages to correct topic */
  unsigned short br_id = get_param(chunk,"Border_Router_ID");

  if (border_router_packet.mqtt_border_router_id == 0) {
    LOG_INFO("The user has just entered in the first room after the booting up.\n");
  } else if (border_router_packet.mqtt_border_router_id != br_id){
    LOG_INFO("The user has just changed room.\n");
  }

  border_router_packet.mqtt_border_router_id = br_id;

  /* Now we can unsubscribe from the sub topic since we have the needed infos */
  unsubscribe();

  if(construct_pub_topic() == 0) {
    /* Fatal error. Topic larger than the buffer */
    state = STATE_CONFIG_ERROR;
    return;
  } else {
    state = STATE_PUBLISHING;
  }

  /* If the format != json, ignore */
  if(strncmp(&topic[topic_len - 4], "json", 4) != 0) {
    LOG_ERR("Incorrect format\n");
    return;
  }
}
/*---------------------------------------------------------------------------*/
/*
 * Callback procedure for mqtt.
 */
static void
mqtt_event(struct mqtt_connection *m, mqtt_event_t event, void *data)
{
  switch(event) {
    case MQTT_EVENT_CONNECTED: {
      LOG_INFO("Application has a MQTT connection!\n");
      timer_set(&connection_life, CONNECTION_STABLE_TIME);
      state = STATE_CONNECTED;

      break;
    }
    case MQTT_EVENT_DISCONNECTED: {
      LOG_INFO("MQTT Disconnect: reason %u\n", *((mqtt_event_t *)data));

      state = STATE_DISCONNECTED;
      process_poll(&mqtt_client_process);
      break;
    }
    case MQTT_EVENT_PUBLISH: {
      msg_ptr = data;

    /* Implement first_flag in publish message? */
      if(msg_ptr->first_chunk) {
        msg_ptr->first_chunk = 0;
        LOG_INFO("Application received a publish on topic '%s'; payload "
          "size is %i bytes\n",
          msg_ptr->topic, msg_ptr->payload_length);
      }

      pub_handler(msg_ptr->topic, strlen(msg_ptr->topic), msg_ptr->payload_chunk,
        msg_ptr->payload_length);

      temptime = (unsigned long)(ENERGEST_GET_TOTAL_TIME() / ENERGEST_SECOND);
      conntime = temptime - conntime;
      
      // LOG_INFO("conntime:%lu\n", conntime);

      break;
    }
    case MQTT_EVENT_SUBACK: {
      LOG_INFO("Application is subscribed to topic successfully\n");
      break;
    }
    case MQTT_EVENT_UNSUBACK: {
      LOG_INFO("Application is unsubscribed to topic successfully\n");
      break;
    }
    case MQTT_EVENT_PUBACK: {
      LOG_INFO("Publishing complete\n");

      estimationtime = (unsigned long)(ENERGEST_GET_TOTAL_TIME() / ENERGEST_SECOND) - temptime;
      totaltime += estimationtime;
      totalcounter += 1;
      temptime = (unsigned long)(ENERGEST_GET_TOTAL_TIME() / ENERGEST_SECOND);

      // LOG_INFO("estimationtime:%lu\n", estimationtime);
      // LOG_INFO("totaltime:%lu\n", totaltime);
      // LOG_INFO("totalcounter:%lu\n", totalcounter);

      break;
    }
    default:
    LOG_WARN("Application got a unhandled MQTT event: %i\n", event);
    break;
  }
}
/*---------------------------------------------------------------------------*/
/*
 * Procedure that constructs the subscribing topic.
 */
static int
construct_sub_topic(void)
{
  int len = snprintf(sub_topic, BUFFER_SIZE, MQTT_SUBSCRIBE_TOPIC);

  /* len < 0: Error. Len >= BUFFER_SIZE: Buffer too small */
  if(len < 0 || len >= BUFFER_SIZE) {
    LOG_INFO("Sub topic: %d, buffer %d\n", len, BUFFER_SIZE);
    return 0;
  }

  return 1;
}
/*---------------------------------------------------------------------------*/
/*
 * Procedure that constructs the IPv6 address.
 */
static int
construct_client_id(void)
{
  int len = snprintf(client_id, BUFFER_SIZE, "d:%s:%s:%02x%02x%02x%02x%02x%02x",
   DEFAULT_ORG_ID, DEFAULT_TYPE_ID,
   linkaddr_node_addr.u8[0], linkaddr_node_addr.u8[1],
   linkaddr_node_addr.u8[2], linkaddr_node_addr.u8[5],
   linkaddr_node_addr.u8[6], linkaddr_node_addr.u8[7]);

  /* len < 0: Error. Len >= BUFFER_SIZE: Buffer too small */
  if(len < 0 || len >= BUFFER_SIZE) {
    LOG_INFO("Client ID: %d, Buffer %d\n", len, BUFFER_SIZE);
    return 0;
  }

  return 1;
}
/*---------------------------------------------------------------------------*/
/*
 * Procedure that check that initializes the sequence number,
 * checks that the IPv6 address and the subscribing topic
 * are consctructed correctly.
 * It triggers the first timer event.
 * It also set the state of our mqtt connection.
 */
static void
update_config(void)
{
  if(construct_client_id() == 0) {
    /* Fatal error. Client ID larger than the buffer */
    state = STATE_CONFIG_ERROR;
    return;
  }

  if(construct_sub_topic() == 0) {
    /* Fatal error. Topic larger than the buffer */
    state = STATE_CONFIG_ERROR;
    return;
  }

  /* Reset the counter */
  seq_nr_value = 0;

  state = STATE_INIT;

  /*
   * Schedule next timer event ASAP
   *
   * If we entered an error state then we won't do anything when it fires.
   *
   * Since the error at this stage is a config error, we will only exit this
   * error state if we get a new config.
   */
  etimer_set(&publish_periodic_timer, 0);

  return;
}
/*---------------------------------------------------------------------------*/
/*
 * It subscribes the mobile device over a topic.
 */
static void
subscribe(void)
{
  mqtt_status_t status;

  status = mqtt_subscribe(&conn, NULL, sub_topic, MQTT_QOS_LEVEL_0);

  LOG_INFO("Subscribing\n");
  if(status == MQTT_STATUS_OUT_QUEUE_FULL) {
    LOG_INFO("Tried to subscribe but command queue was full!\n");
  }
}
/*---------------------------------------------------------------------------*/
/*
 * It publishes a payload message over a topic.
 */
static void
publish(void)
{
  mqtt_status_t conn_status;

  /* Publish MQTT topic */
  int len;
  int remaining = APP_BUFFER_SIZE;

  seq_nr_value++;

  buf_ptr = app_buffer;

  len = snprintf(buf_ptr, remaining,
   "{"
   "\"Payload\":{"
   "\"Mobile_Device_ID\":%d,"
   "\"Border_Router_ID\":%d,"
   "\"Device_Type\":\"%s\","
   "\"Seq #\":%d,"
   "\"Uptime (sec)\":%lu,"
   "\"Acceleration_value\":%d",
   MOBILE_DEVICE_ID,
   border_router_packet.mqtt_border_router_id, 
   DEFAULT_TYPE_ID,
   seq_nr_value, 
   clock_seconds(), 
   border_router_packet.acceleration_mean); 

  if(len < 0 || len >= remaining) {
    LOG_ERR("Buffer too short. Have %d, need %d + \\0\n", remaining, len);
    return;
  }

  remaining -= len;
  buf_ptr += len;

  /* Put our Default route's string representation in a buffer */
  char def_rt_str[64];
  memset(def_rt_str, 0, sizeof(def_rt_str));
  ipaddr_snprintf(def_rt_str, sizeof(def_rt_str), uip_ds6_defrt_choose());

  len = snprintf(buf_ptr, remaining, ",\"Def Route\":\"%s\"",
   def_rt_str);

  if(len < 0 || len >= remaining) {
    LOG_ERR("Buffer too short. Have %d, need %d + \\0\n", remaining, len);
    return;
  }
  remaining -= len;
  buf_ptr += len;

  len = snprintf(buf_ptr, remaining, "}}");

  if(len < 0 || len >= remaining) {
    LOG_ERR("Buffer too short. Have %d, need %d + \\0\n", remaining, len);
    return;
  }

  conn_status = mqtt_publish(&conn, NULL, pub_topic, (uint8_t *)app_buffer,
   strlen(app_buffer), MQTT_QOS_LEVEL_1, MQTT_RETAIN_OFF);
  LOG_INFO("State of mqtt_publish: %i\n", conn_status);

  LOG_INFO("Publish sent out!\n");
}
/*---------------------------------------------------------------------------*/
/*
 * It tries to connect the mobile device to the broker.
 */
static void
connect_to_broker(void)
{
  mqtt_status_t conn_status;

  /* Connect to MQTT server */
  conn_status = mqtt_connect(&conn, MQTT_BROKER_IP_ADDR, DEFAULT_BROKER_PORT,
   K * 3);
  LOG_INFO("State of mqtt_connect: %i\n", conn_status);

  state = STATE_CONNECTING;
}
/*---------------------------------------------------------------------------*/
/*
 * Main procedure that manages the interaction with the broker.
 */
static void
state_machine(void)
{
  mqtt_status_t conn_status;

  switch(state) {
    case STATE_INIT:

    
    /* If we have just been configured register MQTT connection */
    conn_status = mqtt_register(&conn, &mqtt_client_process, client_id, mqtt_event,
      MAX_TCP_SEGMENT_SIZE);
    LOG_INFO("State of mqtt_register: %i\n", conn_status);

    // mqtt_set_username_password(&conn, "use-token-auth",
    //  DEFAULT_AUTH_TOKEN);

    /* _register() will set auto_reconnect. We don't want that. */
    conn.auto_reconnect = 0;
    connect_attempt = 1;

    state = STATE_REGISTERED;
    LOG_INFO("Init\n");
    /* Continue */
    case STATE_REGISTERED:
    if(uip_ds6_get_global(ADDR_PREFERRED) != NULL) {
      /* Registered and with a global IPv6 address, connect! */
      LOG_INFO("Joined network! Connect attempt %u\n", connect_attempt);
      connect_to_broker();
    } else {
      leds_on(MQTT_STATUS_LED);
      ctimer_set(&ct, NO_NET_LED_DURATION, publish_led_off, NULL);
    }
    etimer_set(&publish_periodic_timer, NET_CONNECT_PERIODIC);
    return;
    break;
    case STATE_CONNECTING:
    leds_on(MQTT_STATUS_LED);
    ctimer_set(&ct, CONNECTING_LED_DURATION, publish_led_off, NULL);
    /* Not connected yet. Wait */
    LOG_INFO("Connecting: retry %u...\n", connect_attempt);
    break;
    case STATE_CONNECTED:
      if(mqtt_ready(&conn) && conn.out_buffer_sent) {
        subscribe();  
        etimer_set(&publish_periodic_timer, K);
      } else {
        LOG_INFO("Subscribing... (MQTT state=%d, q=%u)\n", conn.state, 
          conn.out_queue_full);
      }
      
      LOG_INFO("Waiting for data...\n");

    return;
    break;
    case STATE_PUBLISHING:
    /* If the timer expired, the connection is stable. */
    if(timer_expired(&connection_life)) {
      /*
       * Intentionally using 0 here instead of 1: We want RECONNECT_ATTEMPTS
       * attempts if we disconnect after a successful connect
       */
      connect_attempt = 0;
    }

    if(mqtt_ready(&conn) && conn.out_buffer_sent) {

      leds_on(MQTT_STATUS_LED);
      ctimer_set(&ct, PUBLISH_LED_ON_DURATION, publish_led_off, NULL);
      publish();
      
      etimer_set(&publish_periodic_timer, K);

      /* Return here so we don't end up rescheduling the timer */
      return;
    } else {
      /*
       * Our publish timer fired, but some MQTT packet is already in flight
       * (either not sent at all, or sent but not fully ACKd).
       *
       * This can mean that we have lost connectivity to our broker or that
       * simply there is some network delay. In both cases, we refuse to
       * trigger a new message and we wait for TCP to either ACK the entire
       * packet after retries, or to timeout and notify us.
       */
      LOG_INFO("Publishing... (MQTT state=%d, q=%u)\n", conn.state,
        conn.out_queue_full);
    }
    break;
    case STATE_DISCONNECTED:
    LOG_INFO("Disconnected\n");
    if(connect_attempt < RECONNECT_ATTEMPTS ||
     RECONNECT_ATTEMPTS == RETRY_FOREVER) {
      /* Disconnect and backoff */
      clock_time_t interval;
    mqtt_disconnect(&conn);
    connect_attempt++;

    interval = connect_attempt < 3 ? RECONNECT_INTERVAL << connect_attempt :
    RECONNECT_INTERVAL << 3;

    LOG_INFO("Disconnected: attempt %u in %lu ticks\n", connect_attempt, interval);

    etimer_set(&publish_periodic_timer, interval);

    state = STATE_REGISTERED;
    return;
    } else {
      /* Max reconnect attempts reached. Enter error state */
    state = STATE_ERROR_2;
    LOG_ERR("Aborting connection after %u attempts\n", connect_attempt - 1);
    }
    break;
    case STATE_CONFIG_ERROR:
    /* Idle away. The only way out is a new config */
    LOG_ERR("Bad configuration.\n");
    return;
    case STATE_ERROR_2:
    default:
    leds_on(MQTT_STATUS_LED);
    /*
     * 'default' should never happen.
     *
     * If we enter here it's because of some error. Stop timers. The only thing
     * that can bring us out is a new config event
     */
    LOG_INFO("Default case: State=0x%02x\n", state);
    return;
  }

  /* If we didn't return so far, reschedule ourselves */
  etimer_set(&publish_periodic_timer, STATE_MACHINE_PERIODIC);
}
/*---------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(mqtt_client_process, ev, data)
{

  PROCESS_BEGIN();

/*---------------------------------------------------------------------------*/
  /*
   * We start-up the protothreads responsible to manage TSCH.
   * They also switch on the radio. 
   */
  // Process tx/rx callback and log messages whenever polled
  process_start(&tsch_pending_events_process, NULL);
  // periodically send TSCH EBs 
  process_start(&tsch_send_eb_process, NULL);
  // try to associate to a network or start one if setup as coordinator
  process_start(&tsch_process, NULL);
/*---------------------------------------------------------------------------*/

  border_router_packet = * (mqtt_response_packet_t *) data;

  /*
   * It is set the first G value to the default one.
   */
  // if (!(border_router_packet.estimatedG > 0)) 
  border_router_packet.estimatedG = G;

  /* It is taken the time of when the radio is switched on */
  conntime = (unsigned long)(ENERGEST_GET_TOTAL_TIME() / ENERGEST_SECOND);
  
  temptime = 0;
  estimationtime = 0;
  totaltime = 0;
  totalcounter = 0;

  etimer_set(&user_return_to_move, border_router_packet.estimatedG);

  LOG_INFO("MQTT Client Process\n");
  LOG_INFO("Mobile Device ID: %d\n", MOBILE_DEVICE_ID);
  LOG_INFO("Default Value of G: %d\n", G);
  LOG_INFO("Data value sent: %d\n", border_router_packet.acceleration_mean);

  update_config();

  /* Main loop */
  while(1) {

    if (etimer_expired(&user_return_to_move)) {
      if (totalcounter > 0) {
        /* It is updated the parameter of the estimation of G, if
         * there exists one.
         */
        border_router_packet.estimatedG = (conntime 
          + (totaltime / totalcounter)) * CLOCK_SECOND;
      } else {
        /* re-fresh G in case of no publishing before */
        border_router_packet.estimatedG = G;
      }

      LOG_INFO("G estimation: %lu\n", border_router_packet.estimatedG);

      /* Is is given back the control to the sampling protothread */
      process_post(&sampling_client_process, PROCESS_EVENT_POLL, &border_router_packet);
      PROCESS_WAIT_EVENT_UNTIL(ev == PROCESS_EVENT_POLL && data != NULL);

      /*
       * The mqtt protothread is resumed and we re-initialize the counters
       * necessary to estimate G.
       */
      LOG_INFO("mqtt-client protothread resumed\n");

      border_router_packet = * (mqtt_response_packet_t *) data;
      temptime = 0;
      conntime = (unsigned long)(ENERGEST_GET_TOTAL_TIME() / ENERGEST_SECOND);
      estimationtime = 0;
      totaltime = 0;
      totalcounter = 0;

      etimer_set(&user_return_to_move, border_router_packet.estimatedG);

      LOG_INFO("Data value sent: %d\n", border_router_packet.acceleration_mean);

      update_config();
    }

    PROCESS_YIELD();

    if (ev == PROCESS_EVENT_TIMER && data == &publish_periodic_timer) {
      state_machine();
    }
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
/**
 * @}
 * @}
 **/
 