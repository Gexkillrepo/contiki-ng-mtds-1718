/*---------------------------------------------------------------------------*/
/** 
 * \energest-client.c
 * Process for a Mobile Device that keeps track of energy consumption 
 * measured in time (milliseconds) and energy (microJoule).
 */
/*---------------------------------------------------------------------------*/
#include "contiki.h"
#include "sys/clock.h"
#include "sys/etimer.h"
#include "sys/energest.h"

#include <stdio.h>  /* For printf() */

/*---------------------------------------------------------------------------*/
PROCESS(energest_client_process, "Energest client process");
PROCESS_NAME(sampling_client_process);
AUTOSTART_PROCESSES(&sampling_client_process, &energest_client_process);
/*---------------------------------------------------------------------------*/

static void energest_status_info(void)
{
  /* Flush all energest times so we can read latest values */
  energest_flush();

  /*
   * The moltiplication by a factor of 1000 in the following calculations
   * is to allow to express the time in milliseconds, instead of seconds  
   * since the results obtained experimentally are very low.
   */
  unsigned long cpu_active = (unsigned long)(energest_type_time(ENERGEST_TYPE_CPU) 
    * 1000 / ENERGEST_SECOND);
  unsigned long cpu_lpm = (unsigned long)(energest_type_time(ENERGEST_TYPE_LPM) 
    * 1000 / ENERGEST_SECOND);
  unsigned long cpu_deeplpm = (unsigned long)(energest_type_time(ENERGEST_TYPE_DEEP_LPM) 
    * 1000 / ENERGEST_SECOND);
  unsigned long radio_listen = (unsigned long)(energest_type_time(ENERGEST_TYPE_LISTEN) 
    * 1000 / ENERGEST_SECOND);
  unsigned long radio_transmit = (unsigned long)(energest_type_time(ENERGEST_TYPE_TRANSMIT) 
    * 1000 / ENERGEST_SECOND);
  unsigned long radio_off = (unsigned long)((ENERGEST_GET_TOTAL_TIME() * 1000 
    - energest_type_time(ENERGEST_TYPE_LISTEN) * 1000
    - energest_type_time(ENERGEST_TYPE_TRANSMIT) * 1000) / ENERGEST_SECOND);
  unsigned long totaltime = (unsigned long)(ENERGEST_GET_TOTAL_TIME() 
    * 1000 / ENERGEST_SECOND);
  unsigned long energy_rx = radio_listen * POWER_RX;
  unsigned long energy_tx = radio_transmit * POWER_TX;
  unsigned long energy_off = radio_off * POWER_ACTIVE_CPU;
  
  printf("\nENERGY MONITORING:\n");
  printf("TOTAL TIME: %4lums\n", totaltime);

  printf("CPU: Active: %4lums    LPM: %4lums    Deep LPM: %4lums\n", 
    cpu_active, cpu_lpm, cpu_deeplpm);

  printf("RADIO: LISTEN: %4lums    Transmit: %4lums    OFF: %4lums\n",
    radio_listen, radio_transmit, radio_off);

  printf("ENERGY CONSUMPTION: Radio RX: %4luuJ    Radio TX: %4luuJ    Radio Off: %4luuJ\n",
    energy_rx, energy_tx, energy_off);
}
/*---------------------------------------------------------------------------*/
PROCESS_THREAD(energest_client_process, ev, data)
{
  static struct etimer et;
  PROCESS_BEGIN();

  etimer_set(&et, ENERGEST_DELAY);

  while(1) {
    PROCESS_WAIT_EVENT_UNTIL(etimer_expired(&et));
    etimer_reset(&et);

    energest_status_info();
  }

  PROCESS_END();
}
/*---------------------------------------------------------------------------*/
/**
 * @}
 * @}
 **/